FROM training.btech.id/btacademy/httpd:latest

RUN apt update

RUN apt -y install curl software-properties-common

RUN apt -y install php php-cli php-common php-mbstring php-cli php-fpm php-json php-pdo php-mysql php-zip php-gd  php-mbstring php-curl php-xml php-pear php-bcmath 

RUN apt -y install libapache2-mod-php

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /home/apps

WORKDIR /home/apps

RUN cp .env.example .env

RUN composer install
RUN    php artisan optimize:clear
RUN    php artisan key:generate
RUN    php artisan migrate
RUN    php artisan db:seed


CMD ["php", "artisan", "serve", "--host=0.0.0.0"]
#RUN php artisan serve
#RUN service apache2 restart
#RUN service apache2 status

EXPOSE 8000
